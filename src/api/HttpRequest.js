import axios from 'axios';
import md5 from 'crypto-js/md5';

class HttpRequest {
  constructor() {
    this._cancelTokenList = {};
  }

  /**
   *
   * @param {string} token Authentication token headers
   * @param {string} device device-code headers
   */
  config(token, device, timeout = 15000) {
    this._token = token;
    this._device = device;
    this._timeout = timeout;
  }

  /**
   * Cancelable request
   * @param {import('axios').AxiosRequestConfig} config Axios request config
   * @param {string|undefined} key  Specified key for request. That using to identify request
   * @returns
   */
  request({ url, method, data, params, headers }, key) {
    if (!key) {
      key = md5(
        //abc
        JSON.stringify({
          url,
          method,
        }),
      ).toString();
    }

    // get last request
    const lastReq = this._cancelTokenList[key];

    if (lastReq) {
      // cancel last request
      lastReq.cancel();
      delete this._cancelTokenList[key];
    }

    const req = this._call({
      url,
      method,
      data,
      params,
      headers,
      cancelToken: new axios.CancelToken((cancel) => {
        this._cancelTokenList[key] = { cancel };
      }),
    });
    req.finally(() => {
      delete this._cancelTokenList[key];
    });
    return req;
  }

  _call({ headers, ...rest }) {
    let token = this._token || '';
    const device = this._device || '';

    if (token.toLowerCase().indexOf('bearer') === -1) {
      token = 'Bearer ' + token;
    }

    return axios
      .request({
        ...rest,
        headers: {
          'Content-Type': 'application/json',
          'DEVICE-CODE': device,
          Authorization: token,
          ...headers,
        },
        timeout: this._timeout,
      })
      .then((res) => res.data)
      .then((res) => {
        if (res.code !== 200) {
          throw res;
        }
        return res;
      })
      .catch((err) => {
        if (axios.isCancel(err)) {
          err.isCancel = true;
        }
        if (err?.message?.toLowerCase().includes('timeout')) {
          err.isTimeout = true;
        }
        throw err;
      });
  }
}

export default new HttpRequest();
