import { HashRouter as BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from './Home';

export default function MainRoute() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}
