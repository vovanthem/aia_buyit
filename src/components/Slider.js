import React from 'react';
import Slider from 'react-slick';
import styled from 'styled-components';

function SliderItem({ imageList }) {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <div>
      <StyleSlider {...settings}>
        {imageList?.length
          ? imageList?.map((elm) => {
              return (
                <div key={elm}>
                  <img src={elm} alt="imageProduct" />
                </div>
              );
            })
          : ''}
      </StyleSlider>
    </div>
  );
}

export default SliderItem;

const StyleSlider = styled(Slider)`
  text-align: center;
  align-items: center;
  height: 16em;
  .slick-prev {
    z-index: 1;
    left: 0px;
  }
  .slick-next {
    z-index: 1;
    right: 0px;
  }
  .slick-dots {
    bottom: 0px;
  }
  img {
    width: 100%;
    height: 16em;
    object-fit: cover;
  }
`;
