const connect = require('connect');
const serveStatic = require('serve-static');

connect()
  .use(serveStatic(__dirname))
  .listen(3100, () => {
    const url = 'http://localhost:3100';
    const start =
      process.platform === 'darwin'
        ? 'open'
        : process.platform === 'win32'
        ? 'start'
        : 'xdg-open';
    require('child_process').exec(start + ' ' + url);
    console.log('Server running on http://localhost:3100/');
  });
