import { ReactComponent as LoadingSvgIcon } from '../../assets/loading.svg';
import { StyleDivIcon, LoadingIconContainer } from './styled';

export default function LoadingIcon({ size = 'default', ...rest }) {
  return (
    <StyleDivIcon>
      <LoadingIconContainer size={size} {...rest}>
        <LoadingSvgIcon />
      </LoadingIconContainer>
    </StyleDivIcon>
  );
}
