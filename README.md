# Getting Started

This project is a base of AIA project started by Ibenefit.vn.

Your application will be run on Ibe Webview Environment (IWE). IWE provides some permissions and resources, that you have to register with Ibe.

## Start development server with fake IWE:

Create a file with name `.env` like `.env.example` file

In project directory, run: 
```
npm install
```
or
```
yarn install
```
to install node_modules.
Run:
```
npm start
```
or
```
yarn start
```
to start dev server.
After your browser opens `http://localhost:3100`, change the permissions that you need then click [OPEN] button. A child window will be opened ReactJs app.

## SystemCore

In this project, we have an instance of `class SystemCore` inside `core` folder. This class provide several methods for communicating with IWE.

### `SystemCore.isReady`
```ts
public isReady: boolean;
```
This property return `true` if SystemCore is ready to communicate and return `false` if not.

### Ready event (Production only)
The event `ready` has been emitted when `SystemCore` is ready to communicate with IWE.

**NOTE:** This event is only emitted on production environment. On production environment it take a short time to create communicating channel. On development environment, if your application has been started by an IWE faker, `SystemCore.isReady` is always return `true` and always return `false` if not. You must handle `ready` event when your application start. See file: `App.js`.
```js
class App extends React.Component {
  state = {
    isReady: SystemCore.isReady,
  };

  componentDidMount() {
    if (!SystemCore.isReady) {
      SystemCore.on('ready', this._handleSystemReady);
      return;
    }
  }

  componentWillUnmount() {
    SystemCore.removeEventListener('ready', this._handleSystemReady);
  }

  _handleSystemReady = () => {
    this.setState({ isReady: SystemCore.isReady });
  };

  render() {
    return (
      <AppContext.Provider
        value={{
          ...this.state,
        }}>
        {this.state.isReady ? (
          <Root>
            <MainRoute />
          </Root>
        ) : (
          <LoadingRoot>
            <LoadingIcon />
          </LoadingRoot>
        )}
      </AppContext.Provider>
    );
  }
}
```
We recommend you keep `App.js` not modified.

### `SystemCore.requestPermission()`
```ts
function requestPermission(permission: string): Promise<{
  permission: string;
  success: boolean;
  data?: any
}>;
```
If you want to access permissions and resources, you must use the method: `SystemCode.requestPermission`.  
- Example: You want to access read user's contacts list, you maybe call:
```js
SystemCore.requestPermission(Permission.READ_CONTACTS)
  .then((res) => {
    console.log('request contacts permission response =>', res); // contacts data result here.
  })
  .catch((err) => {
    console.error(err)  // access is denied
  })
```
If your application has registered `READ_CONTACTS` permission and the user allows it, use `then` to handle the result, in other ways use `catch` to handle the error.

### `SystemCore.exit()`
```ts
function exit():Promise<void>;
```
When you call `exit` function, It will send an exit request to IWE. Your application will be stop and back to Ibe App.  
Usage:
```
SystemCore.exit();
```
### `SystemCore.send()`
```ts
function send(type: string, payload: any): Promise<{
  type: string;
  payload: any;
}>;
```
2 functions: `requestPermission` and `exit` has been implement from function `send`. Function `send` is base method to communicates with IWE. You can send any `type` in `acceptedTypes` to IWE and handle the response by yourself.

## How to create production bundle file

After you built application with react-script:
```
npx run build
```
or
```
yarn build
```
, you must compress all your resources into a file `.html`.  
We can use [gulpjs](https://gulpjs.com/) to do this automatically. See example file: `gulpfile.js`

### Compress custom fonts

`gulpjs` run following tasks list, a task looks like:
```js
task('fonts', () => {
  return src(['./build/static/media/*'])
    .pipe(inlineFonts({ name: 'ibenefit' }))
    .pipe(dest('./build/static/css/'));
});
```
(See more at [gulpjs.com](https://gulpjs.com/))
In this task, we compress all custom font in directory: `build/static/media/` to only one file `./build/static/css/ibenefit.css`. After you have `.css` file you can use compress css files.

### Compress js and css
You can compress your custom js and css after react-script built.
First inject your custom js and css to `build/index.html` or `public/index.html`. **Note**: if you inject js and css to `build/index.html` it will be removed after each built time.

In this example we injected a custom css file into `public/index.html`:

```
  <link rel="stylesheet" href="/static/css/ibenefit.css" />
```

Then, you add this gulp task to inline all css and js file what injected into `index.html`

```js
task('inline', () => {
  return src('./build/index.html')
    .pipe(replace('.js"></script>', '.js" inline></script>'))
    .pipe(replace('rel="stylesheet"', 'rel="stylesheet" inline'))
    .pipe(
      inlinesource({
        compress: false,
        ignore: ['png'],
      }),
    )
    .pipe(dest('./dist'));
});
```

## Compress `index.html`
After you create a gulpfile.js, run this command
```
npm run dist
```
or
```
yarn dist
```

to create only one `index.html` file.

You will get a `index.html` file in `dist/` directory.

## Publishing your application

Upload your `index.html` file to AIA server, adding permission what you need and wait we approve it.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Create ReactJS app at [http://localhost:3000](http://localhost:3000) then open fake IWE at [http://localhost:3100](http://localhost:3100) to view it in the browser.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn dist`

Create bundle file using `gulp`:
- Create Ibenefit Icons Font inline css file.
- Inject all static resources (css, js) to `index.html`.

Learn more: [gulp](https://gulpjs.com/)

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
