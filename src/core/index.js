import MessageTypes from './constants/message-types';
import {
  NotAllowedType,
  RequestPermissionFailed,
  SystemNotReadyError,
} from './utils/errors';
import EventEmitter from './utils/event-emitter';

/**
 *
 * @param {string} message
 * @returns {Promise<string | {type: string; payload: any} | undefined>}
 */
function sendMessageToFlutter(message) {
  return window.flutter_inappwebview?.callHandler?.('Flutter', message);
}

class SystemCore extends EventEmitter {
  get acceptedTypes() {
    return this._acceptedTypes;
  }

  get isReady() {
    return this._isReady;
  }

  constructor() {
    super();
    this._isReady = !window.flutter_inappwebview ? !!window.opener : false;
    this._acceptedTypes = Object.values(MessageTypes);
    this._subscribe();
  }

  async send(type, payload = {}) {
    if (!this.isReady) {
      throw new SystemNotReadyError();
    }
    if (!this._checkIsAllowType(type)) {
      throw new NotAllowedType();
    }
    const msg = JSON.stringify({
      type,
      payload,
    });
    if (window.flutter_inappwebview) {
      const res = await sendMessageToFlutter(msg);
      if (typeof res === 'string') {
        return this._handleJsonStringMessage(res);
      }
      if (res?.type !== type) {
        return;
      }
      this.emit(res?.type || type, res);
      return res;
    }
    return await this._postMessageToWindow(type, msg);
  }

  async exit() {
    await this.send(MessageTypes.EXIT);
  }

  async requestPermission(permission, option = undefined) {
    const res = await this.send(MessageTypes.PERMISSION, {
      permission,
      option,
    });
    if (!res?.payload?.success) {
      const error = new RequestPermissionFailed();

      if (!res?.payload) {
        error.success = false;
        error.permission = permission;
        throw error;
      }
      Object.assign(error, res.payload);
      throw error;
    }
    res.payload.permission = permission;
    return res.payload;
  }

  _postMessageToWindow(type, message) {
    const _this = this;
    return new Promise((resolve) => {
      const handleReceivedResponse = function (res) {
        resolve(res);
        this.removeEventListener(type, handleReceivedResponse);
      }.bind(_this);

      this.on(type, handleReceivedResponse);
      window.opener?.postMessage(message, '*');
    });
  }

  _checkIsAllowType(type) {
    return this._acceptedTypes.includes(type);
  }

  _subscribe() {
    window.addEventListener('flutterInAppWebViewPlatformReady', () => {
      this._isReady = true;
      this.emit('ready');
    });
    window.addEventListener('message', (ev) => {
      const { data } = ev;
      if (typeof data === 'string') {
        return this._handleJsonStringMessage(data);
      }
      if (this._checkIsAllowType(data?.type)) {
        this.emit(data?.type, data);
      }
    });
  }

  /**
   *
   * @param {string} stringData
   * @returns {Promise<{type: string; payload: any} | undefined>}
   */
  _handleJsonStringMessage(stringData) {
    if (!stringData) {
      return;
    }
    try {
      const data = JSON.parse(stringData);
      if (this._checkIsAllowType(data.type)) {
        this.emit(data.type, data);
        return data;
      }
    } catch (err) {
      //
    }
  }
}

const core = new SystemCore();
export { core as SystemCore };
