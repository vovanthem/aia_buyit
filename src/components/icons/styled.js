import styled, { keyframes } from 'styled-components';

const rotate = keyframes`
from {
  transform: rotate(0deg);
}

to {
  transform: rotate(360deg);
}
`;

export const StyleDivIcon = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: var(--hint-primary-color);
`;

export const LoadingIconContainer = styled.span`
  width: var(--icon-size-${({ size }) => size});
  height: var(--icon-size-${({ size }) => size});
  display: inline-block;
  animation: ${({ loading = true }) => loading && rotate} 1.25s linear infinite;
  color: var(--primary-color);
  margin-top: var(--space-0);
  svg {
    width: 100%;
    height: 100%;
  }
`;
