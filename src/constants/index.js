const appConstants = {
  apiUrl: process.env.REACT_APP_ENDPOINT,
};

export default appConstants;
