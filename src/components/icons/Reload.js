import { ReactComponent as ReloadSvgIcon } from '../../assets/reload.svg';
import { LoadingIconContainer } from './styled';

export default function ReloadIcon({ children, ...rest }) {
  return (
    <LoadingIconContainer {...rest}>
      <ReloadSvgIcon />
    </LoadingIconContainer>
  );
}
