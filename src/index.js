import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import './assets/ibe-icons.css';
import { SystemCore } from './core';
import { ToastContainer } from 'react-toastify';
import './assets/ibe-icons.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import 'react-toastify/dist/ReactToastify.css';

if (!SystemCore) {
  // console.error('System has not working');
}

ReactDOM.render(
  // <React.StrictMode>
  <>
    <App />
    <ToastContainer
      position="top-center"
      hideProgressBar
      toastClassName="toast"
      limit={3}
      autoClose={3000}
    />
  </>,
  // </React.StrictMode>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
