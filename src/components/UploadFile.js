import React, { Component } from 'react';
import styled from 'styled-components';
import UploadFile from '../utils/UploadFile';
import LoadingIcon from './icons/Loading';

class UploadFileComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
    this.refInput = React.createRef();
  }

  handleChange = (e) => {
    this.setState({ loading: true });
    this.props.disableBtnNex();
    UploadFile(e)
      .then((res) => {
        if (res?.status !== 200) {
          throw res;
        }
        const { data } = res.data;
        this.setState({ loading: false });
        this.props.onChange(data);
        this.props.disableBtnNex();
      })
      .catch((err) => {
        this.props.disableBtnNex();
        this.setState({ loading: false });
      });
  };

  handleClick = () => {
    this.refInput.current.value = null;
    this.refInput.current.click();
  };

  componentWillUnmount() {
    clearInterval();
  }

  render() {
    let { disable, accept } = this.props;
    let { loading } = this.state;
    return (
      <>
        {disable ? (
          <>
            <StyleDivInput
              className="App"
              onClick={this.handleClick}
              disabled={loading}>
              {loading && <LoadingIcon loading={1} />}
              <div className="icon icon-camera-plus-ico divIcon" />
              <input
                accept={accept || ''}
                type="file"
                ref={this.refInput}
                onChange={this.handleChange}
                multiple
                name="file"
              />
            </StyleDivInput>
          </>
        ) : null}
      </>
    );
  }
}
export default UploadFileComponent;

const StyleDivInput = styled.button`
  display: flex;
  flex-flow: column;
  width: 100px;
  height: 100px;
  justify-content: center;
  align-items: center;
  border: none;
  border-radius: var(--space-0);
  color: var(--secondary-color);
  position: relative;
  cursor: pointer;
  :hover {
    border: 1px solid var(--primary-color);
  }

  .divIcon {
    font-size: var(--icon-size-large);
  }
  input {
    display: none;
  }
`;
