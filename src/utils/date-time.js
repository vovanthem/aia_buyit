export const MINUTE_TO_SECOND = 60;
export const HOUR_TO_SECOND = MINUTE_TO_SECOND * 60;
export const DAY_TO_SECOND = HOUR_TO_SECOND * 24;
export const WEEK_TO_SECOND = DAY_TO_SECOND * 7;
export const MONTH_TO_SECOND = DAY_TO_SECOND * 30;
export const YEAR_TO_SECOND = DAY_TO_SECOND * 365;

/**
 * add date
 * @param {Date} date instance of Date
 * @param {number} value number of day
 * @returns
 */
export function addDate(date, value) {
  if (!(date instanceof Date)) {
    date = new Date();
  }
  if (!value) {
    return date;
  }
  const millisecondsOfDay = 1000 * 3600 * 24;
  return new Date(date.getTime() + value * millisecondsOfDay);
}

/**
 * format Date: YYYY/MM/DD
 * @param {Date} date
 * @param {string} separate separate to divide date part: '/', '-',...
 */
export function dateToYYYYMMDD(date, separate = '/') {
  const y = date.getFullYear();
  const m = `0${date.getMonth() + 1}`.substr(-2);
  const d = `0${date.getDate()}`.substr(-2);
  return `${y}${separate}${m}${separate}${d}`;
}
/**
 * format Date: DD/MM/YYYY
 * @param {Date} date
 * @param {string} separate separate to divide date part: '/', '-',...
 */
export function dateToDDMMYYYY(date, separate = '/') {
  const y = date.getFullYear();
  const m = `0${date.getMonth() + 1}`.substr(-2);
  const d = `0${date.getDate()}`.substr(-2);
  return `${d}${separate}${m}${separate}${y}`;
}

export function startOf(date, unit = 'd') {
  switch (unit) {
    case 'w':
    case 'week':
      return startOfWeek(date);
    case 'm':
    case 'month':
      return startOfMonth(date);
    case 'd':
    case 'day':
    case 'date':
    default:
      return startOfDay(date);
  }
}

function startOfDay(date) {
  const clone = new Date(date);
  clone.setHours(0, 0, 0, 0);
  return clone;
}

function startOfWeek(date) {
  return startOfDay(addDate(date, -date.getDay() + 1));
}

function startOfMonth(date) {
  return startOfDay(addDate(date, -date.getDate() + 1));
}
