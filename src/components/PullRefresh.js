import { useState } from 'react';
import PullToRefresh from 'react-simple-pull-to-refresh';
import styled from 'styled-components';
import ReloadIcon from './icons/Reload';

const StyledPullToRefresh = styled(PullToRefresh)`
  flex: 1;
  .ptr__children {
    display: flex;
  }
  .ptr__pull-down--pull-more {
    display: none;
  }
`;

export default function ({ isPullable = true, onRefresh, children }) {
  const [refreshing, setRefreshing] = useState();
  function handleRefresh() {
    if (typeof onRefresh === 'function') {
      setRefreshing(1);
      return onRefresh().then(() => {
        setRefreshing(0);
      });
    }
    return Promise.resolve();
  }

  return (
    <StyledPullToRefresh
      isPullable={isPullable}
      onRefresh={handleRefresh}
      refreshingContent={<ReloadIcon loading={refreshing} />}>
      {children}
    </StyledPullToRefresh>
  );
}
