import styled from 'styled-components';
import React from 'react';

const ScrollRoot = styled.div`
  flex: 1;
  position: relative;
  overflow: hidden;
  /* background: #f4f4f4; */
  /* margin-bottom: 3em; */
`;

const Wrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
`;

export default class ScrollViewer extends React.Component {
  _scrollRef = React.createRef();

  componentDidMount() {
    this._invokeLoadMore();
  }

  componentDidUpdate(nextProps) {
    if (nextProps.canLoadMore && !this._loadingMore) {
      window.setTimeout(() => {
        this._checkHasScrollbar();
      });
    }
  }

  _needLoadMore = () => {
    const scroll = this._scrollRef.current;
    if (scroll) {
      const elHeight = scroll.getBoundingClientRect().height;
      return Math.floor(scroll.scrollHeight - elHeight) <= 0;
    }
    return false;
  };

  _invokeLoadMore = () => {
    const { canLoadMore, onLoadMore } = this.props;
    if (canLoadMore && !this._loadingMore) {
      const loadMorePromise = typeof onLoadMore === 'function' && onLoadMore();
      if (loadMorePromise) {
        this._loadingMore = true;
        loadMorePromise.then(() => {
          this._loadingMore = false;
          this._checkHasScrollbar();
        });
      }
    }
  };

  _checkHasScrollbar() {
    if (this._needLoadMore()) {
      this._invokeLoadMore();
    }
  }

  _handleScroll = () => {
    const scroll = this._scrollRef.current;
    if (scroll) {
      const elHeight = scroll.getBoundingClientRect().height;
      const scrollHeight = scroll.scrollHeight - elHeight;
      if (scrollHeight <= 0) {
        return;
      }
      const curPos = scroll.scrollTop;
      if (curPos - (this._prePos || 0) > 0) {
        if (scrollHeight - curPos <= 0.1 * elHeight) {
          this._invokeLoadMore();
        }
      }
      this._prePos = curPos;
    }
  };

  render() {
    const { children, onLoadMore, canLoadMore, ...rest } = this.props;
    return (
      <ScrollRoot className="scroll_viewer-root" {...rest}>
        <Wrapper
          ref={this._scrollRef}
          className="scroll_viewer-container"
          onScroll={this._handleScroll}>
          {children}
        </Wrapper>
      </ScrollRoot>
    );
  }
}
