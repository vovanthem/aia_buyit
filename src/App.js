import React from 'react';
import styled from 'styled-components';
import LoadingIcon from './components/icons/Loading';
import AppContext from './context/AppContext';
import { SystemCore } from './core';
import MainRoute from './page';

const Root = styled.div`
  width: 100vw;
  height: 100vh;
`;

const LoadingRoot = styled(Root)`
  display: flex;
  justify-content: center;
  align-items: center;
`;

class App extends React.Component {
  state = {
    isReady: SystemCore.isReady,
  };

  componentDidMount() {
    if (!SystemCore.isReady) {
      SystemCore.on('ready', this._handleSystemReady);
      return;
    }
  }

  componentWillUnmount() {
    SystemCore.removeEventListener('ready', this._handleSystemReady);
  }

  _handleSystemReady = () => {
    this.setState({ isReady: SystemCore.isReady });
  };

  render() {
    return (
      <AppContext.Provider
        value={{
          ...this.state,
        }}>
        {this.state.isReady ? (
          <Root>
            <MainRoute />
          </Root>
        ) : (
          <LoadingRoot>
            <LoadingIcon />
          </LoadingRoot>
        )}
      </AppContext.Provider>
    );
  }
}

export default App;
