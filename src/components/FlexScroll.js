import styled from 'styled-components';

const Root = styled.div`
  flex: 1;
  position: relative;
  overflow: hidden;
  .flexScroll-content {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    overflow: auto;
  }
`;

export default function ({ children, ...rest }) {
  return (
    <Root {...rest}>
      <div className="flexScroll-content">{children}</div>
    </Root>
  );
}
