import axios from 'axios';

const _uploadFile = async (blob) => {
  const form = new FormData();
  form.append('app_id', 2);
  form.append('file', blob);
  try {
    const result = await axios({
      method: 'POST',
      url: `https://filer.vipn.net/file/`,
      data: form,
    });

    if (!result.data.success) {
      throw result;
    }
    return result;
  } catch (error) {
    // console.log('Lỗi');
  }
};

export default _uploadFile;
